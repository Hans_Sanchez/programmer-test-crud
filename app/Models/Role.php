<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'display_name', 'description'];

    // Método scope para realizar consultas
    public function scopeSearch($query, $search_term)
    {
        $query->Where('name', 'like', '%' . $search_term . '%');
    }

    //Definición de relación con la tabla intermedia
    public function users()
    {
        return $this->belongsToMany(User::class, 'role_user');
    }
}
