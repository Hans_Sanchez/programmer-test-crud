<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

// Paquetes
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
    public function table(Request $request)
    {
        if ($request->ajax()) {

            return DataTables::of(User::with('roles')->select(['id', 'document', 'name', 'email', 'mobile', 'address']))
                ->addColumn('action', function ($users) {
                    $actions = '<button type="button" class="btn btn-warning edit text-white" data-id="' . $users->id . '" title="Editar" data-toggle="modal"
                                                                            data-target=".from-users-edit">
                                    <i class="fa fa-edit"></i>
                                </button>';
                    $actions .= '&nbsp;<button type="button" name="delete" id="" class="btn btn-danger delete" data-id="' . $users->id . '" title="Eliminar">
                                    <i class="fa fa-times"></i>
                                 </button>';

                    return $actions;
                })
                ->addColumn('roles', function (User $user) {
                    return $user->roles->first()->display_name;
                })
                ->rawColumns(['action', 'roles'])
                ->make(true);
        }

        return view('welcome');
    }

    public function store(Request $request)
    {
        if ($request->ajax()) {

            // Validación de campos que llegan el el request

            //Reglas de validación
            $rules = [
                'document' => 'required',
                'name' => 'required',
                'email' => 'required|email|unique:users',
                'password' => 'required|confirmed|min:8',
                'roles' => 'required',
            ];

            //Mensajes personalizados segun cada regla
            $messages = [
                'document.required' => 'El número del documento es requerido. ',
                'name.required' => 'El nombre es requerido. ',
                'email.required' => 'El email es requerido. ',
                'email.email' => 'El campo email no tiene un formato adecuado verifique por favor. ',
                'email.unique' => 'El email que intenta ingresar ya existe, verifique por favor. ',
                'password.required' => 'La contraseña es requerida. ',
                'password.confirmed' => 'Verifique por favor las contraseñas, no son iguales. ',
                'password.min' => 'La contraseña debe contener al menos 8 caracteres. ',
                'roles.required' => 'El rol o el cargo a ejercer es requerido. ',
            ];

            //Función de validación
            $request->validate($rules, $messages);

            //Control de excepciones
            try {

                DB::beginTransaction();
                // Creación del usuario
                $user = User::create([
                    'document' => str_replace('_', '', $request->input('document')), //Limpiando la data
                    'name' => $request->input('name'),
                    'email' => $request->input('email'),
                    'password' => bcrypt($request->input('password')), //Encriptación de la contraseña
                    'mobile' => str_replace('-', '', $request->input('mobile')), //Limpiando la data
                    'address' => $request->input('address'),
                ]);

                //Asiganación del rol o del cargo
                $user->roles()->attach($request->input('roles'));

                //Carga y respuesta exitosa
                DB::commit();
                return response()->json(['user' => $user], 200);

            } catch (\Exception $exception) {

                //Rollback de la información y respuesta del error
                DB::rollBack();
                return response()->json(['msg' => 'Error al cargar la información ' . $exception->getMessage(), 'status' => 'failed'], 500);

            }

        }
    }

    public function update(Request $request, User $user)
    {
        if ($request->ajax()) {

            // Validación de campos que llegan el el request

            //Validación con respecto a si lleva un valor la contraseña
            if ($request->input('password') != null && $request->input('password_confirmation') != null) {
                //Reglas de validación
                $rules = [
                    'document' => 'required',
                    'name' => 'required',
                    'email' => 'required|email|unique:users,email,' . $user->id,
                    'password' => 'required|confirmed|min:8',
                    'roles' => 'required',
                ];

                //Mensajes personalizados segun cada regla
                $messages = [
                    'document.required' => 'El número del documento es requerido. ',
                    'name.required' => 'El nombre es requerido. ',
                    'email.required' => 'El email es requerido',
                    'email.email' => 'El campo email no tiene un formato adecuado verifique por favor',
                    'email.unique' => 'El email que intenta ingresar ya existe, verifique por favor',
                    'password.confirmed' => 'Verifique por favor las contraseñas, no son iguales',
                    'password.min' => 'La contraseña debe contener al menos 8 caracteres',
                    'roles.required' => 'El rol o el cargo a ejercer es requerido. ',
                ];
            } else {
                //Reglas de validación
                $rules = [
                    'document' => 'required',
                    'name' => 'required',
                    'email' => 'required|email|unique:users,email,' . $user->id,
                    'roles' => 'required'
                ];

                //Mensajes personalizados segun cada regla
                $messages = [
                    'document.required' => 'El número del documento es requerido. ',
                    'name.required' => 'El nombre es requerido. ',
                    'email.required' => 'El email es requerido',
                    'email.email' => 'El campo email no tiene un formato adecuado verifique por favor',
                    'email.unique' => 'El email que intenta ingresar ya existe, verifique por favor',
                    'roles.required' => 'El rol o el cargo a ejercer es requerido. ',
                ];
            }

            //Función de validación
            $request->validate($rules, $messages);

            //Control de excepciones
            try {

                DB::beginTransaction();
                // Actualización del usuario
                $user->update([
                    'document' => str_replace('_', '', $request->input('document')), //Limpiando la data
                    'name' => $request->input('name'),
                    'email' => $request->input('email'),
                    'password' => bcrypt($request->input('password')), //Encriptación de la contraseña
                    'mobile' => str_replace('-', '', $request->input('mobile')), //Limpiando la data
                    'address' => $request->input('address'),
                ]);

                //Actualización del rol o del cargo
                $user->roles()->sync($request->input('roles'));

                //Actualización y respuesta exitosa
                DB::commit();
                return response()->json(['user' => $user], 200);

            } catch (\Exception $exception) {

                //Rollback de la información y respuesta del error
                DB::rollBack();
                return response()->json(['msg' => 'Error al actualizar la información ' . $exception->getMessage(), 'status' => 'failed'], 500);

            }

        }
    }

    public function get(User $user)
    {
        return response()->json(User::with('roles')->where('id', $user->id)->first());
    }

    public function destroy(User $user)
    {
        try {
            $user->delete();
            return response()->json(['success', 200]);
        } catch (\Exception $exception) {
            return response()->json(['failed', 500]);
        }
    }
}
