<?php

namespace App\Http\Controllers;

use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;

class RoleController extends Controller
{
    public function index()
    {
        Paginator::useBootstrap();
        $roles = Role::paginate(5);
        return view('welcome', compact('roles'));
    }

    public function store(Request $request)
    {
        try{
            DB::beginTransaction();
            $newRole = new Role();
            $newRole->name = $request->input('name');
            $newRole->display_name = $request->input('display_name');
            $newRole->description = $request->input('description');
            $newRole->save();

            DB::commit();
            \alert()->success("El rol: <b>{$newRole->name}</b> se agregó con éxito");
        }catch(\Exception $e){
            DB::rollBack();
            \alert()->error("Error: ".$e->getMessage());
            return redirect()->back()->withInput($request->all());
        }
        return redirect()->route('welcome');
    }

    public function update(Request $request, Role $role)
    {
        try{

            DB::beginTransaction();

            $role->name = $request->input('name');
            $role->display_name = $request->input('display_name');
            $role->description = $request->input('description');
            $role->update();

            DB::commit();
            \alert()->success("El rol: <b>{$role->display_name}</b> se actualizó con éxito");

        }catch(\Exception $e){

            DB::rollBack();
            \alert()->error("Error: ".$e->getMessage());
            return redirect()->back()->withInput($request->all());

        }

        return redirect()->route('welcome');
    }

    public function destroy(Role $role)
    {
        if ($role->name == "admin") {
            \alert()->error("Error, este rol no se puede eliminar");
            return redirect()->route('admin.roles-index');
        }else{
            try{
                $role->delete();
                \alert()->success("El rol: <b>{$role->display_name}</b> se eliminó con éxito");
            }catch(\Exception $e){
                \alert()->error("Error: ".$e->getMessage());
            }
            return redirect()->route('welcome');
        }
    }

    //Función para traer datos por ajax a un select 2
    public function searchRole(Request $request)
    {
        if ($request->ajax()) {
            $roles = Role::search($request->input('term'))
                ->orderBy('name')
                ->paginate(5);
            $dataJson = array();

            foreach ($roles as $role) {
                $dataJson['results'][] = [
                    'id' => $role->id,
                    'text' => $role->display_name
                ];
            }
            $dataJson['pagination'] = ['more' => $roles->hasMorePages()];

            return response()->json($dataJson);
        }
    }
}
