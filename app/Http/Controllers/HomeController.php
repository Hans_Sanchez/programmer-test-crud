<?php

namespace App\Http\Controllers;

use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;

class HomeController extends Controller
{
    public function welcome(Request $request)
    {
        Paginator::useBootstrap();
        $roles = Role::paginate(5);
        return view('welcome', compact('roles'));
    }
}
