<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>CRUD</title>

    {{-- Icono de aplicación --}}
    <link rel="shortcut icon" href="{{ asset('assets/images/favicon.png') }}">

    {{-- Declaraciones externas para hacer uso Bootstrap CSS --}}
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
          integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <!--Font Awesome-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
          integrity="sha512-+4zCK9k+qNFUR5X+cKL9EIR+ZOhtIloNl9GIKS57V1MyNsYpYcUrUeQc9vNfzsWfV28IaLL3i96P9sdNyeRssA=="
          crossorigin="anonymous"/>

    {{--  Importación de hojas de estilo personalizadas  --}}
    <link rel="stylesheet" href="{{asset('assets/css/app.css')}}">

    {{-- Hojas de estilos de plugins --}}
    <link rel="stylesheet" href="{{asset('assets/plugins/select2/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/plugins/sweetalert2/sweetalert2.min.css')}}">

    {{-- DataTable --}}
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.23/datatables.min.css"/>

</head>
<body>

{{-- row --}}
<div class="row justify-content-center pl-3 pr-3">
    {{-- col --}}
    <div class="col-md-12 pt-3">
        <div class="accordion" id="accordionExample">
            <div class="card">
                <div class="card-header" id="headingOne">
                    <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse"
                                data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            Usuarios
                        </button>
                    </h2>
                </div>

                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                     data-parent="#accordionExample">
                    <div class="card-body">
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-primary mb-3" data-toggle="modal"
                                data-target="#staticBackdrop">
                            Nuevo usuario
                        </button>
                        <br>
                        <!-- End button trigger modal -->

                        <!-- Modal users create -->
                    @include('modals.users-create-modal')
                    <!-- End modal users create -->

                        <!-- Modal users edit -->
                    @include('modals.users-edit-modal')
                    <!-- End modal users edit -->

                        {{-- Listado de usuarios --}}
                        <div class="table-responsive">
                            <table id="users-table" style="width: 100%"
                                   class="table table-bordered table-sm table-hover table-condensed mb-3">
                                <thead>
                                <tr class="bg-primary">
                                    <th class="text-center text-white">
                                        Número de documento
                                    </th>
                                    <th class="text-center text-white">
                                        Nombre del usuario
                                    </th>
                                    <th class="text-center text-white">
                                        Correo electrónico
                                    </th>
                                    <th class="text-center text-white">
                                        Número de teléfono
                                    </th>
                                    <th class="text-center text-white">
                                        Dirección
                                    </th>
                                    <th class="text-center text-white">
                                        Rol o Cargo
                                    </th>
                                    <th class="text-center text-white"
                                        width="70px">
                                        Acciones
                                    </th>
                                </tr>
                                </thead>
                                <tbody class="text-center mb-3"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingTwo">
                    <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left collapsed" type="button"
                                data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false"
                                aria-controls="collapseTwo">
                            Roles
                        </button>
                    </h2>
                </div>
                <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo"
                     data-parent="#accordionExample">
                    <div class="card-body">
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-primary mb-3" data-toggle="modal"
                                data-target="#CreateRole">
                            Nuevo rol
                        </button>
                        <!-- End button trigger modal -->

                        <!-- Modal -->
                    @include('modals.roles-create-modal')
                    <!-- End modal -->

                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr class="bg-primary">
                                    <th class="text-center text-white">Nombre</th>
                                    <th class="text-center text-white">Nombre en pantalla</th>
                                    <th class="text-center text-white">Descripción</th>
                                    <th class="text-center text-white">Opciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($roles as $role)
                                    <tr>
                                        <td>{{$role->name}}</td>
                                        <td>{{$role->display_name}}</td>
                                        <td>{{$role->description}}</td>
                                        <td class="text-center" width="120px">
                                            <div class="btn-group">
                                            @if($role->name != 'admin')

                                                <!-- Button trigger modal -->
                                                    <a data-toggle="modal" title="Editar" style="cursor:pointer;"
                                                       data-target="#EditRole-{{ $role->id }}">
                                                        <i class="fa fa-edit text-warning"></i>
                                                    </a>
                                                    <!-- End button trigger modal -->

                                                    <!-- Modal -->
                                                @include('modals.roles-edit-modal')
                                                <!-- End modal -->
                                                    &nbsp;
                                                    <form method="POST" action="{{ route('roles.destroy', $role)}}"
                                                          style="cursor:pointer;">
                                                        @csrf
                                                        @method('delete')
                                                        <span class="destroy btn-delete" title="Eliminar">
                                                        <i class="fa fa-times text-danger"></i>
                                                    </span>
                                                    </form>
                                                @else
                                                    Sin opciones
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <br>
                            {{$roles->appends(request()->all())->render()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- end col --}}
</div>
{{-- end row --}}


{{-- Inclusión de Sweet Alert--}}
@include('vendor.sweetalert.alert')

{{-- Importación de Jquery --}}
<script src="https://code.jquery.com/jquery-3.1.1.min.js">

    {{-- Declaraciones externas para hacer uso Bootstrap JS --}}
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
            crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js"
        integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s"
        crossorigin="anonymous"></script>

{{-- importación de plugins --}}
<script src="{{asset('assets/plugins/select2/select2.min.js')}}"></script>
<script src="{{asset('assets/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
<script src="{{asset('assets/plugins/select2/js/i18n/es.js')}}"></script>
<script src="{{asset('assets/plugins/inputmask/jquery.inputmask.bundle.min.js')}}"></script>
<script src="{{asset('assets/js/inputmask.js')}}"></script>

{{-- DataTable --}}
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.23/datatables.min.js"></script>


{{-- Implementación AJAX para CRUD --}}
<script>

    // Consulta de roles o cargos mediante AJAX
    $('.roles').select2({
        width: '100%',
        ajax: {
            url: '{{route('roles.searchRoles')}}',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    term: params.term || '',
                    page: params.page || 1
                }
            },
            cache: true
        },
        delay: 250,
        lang: 'es',
        placeholder: "Buscar...",
    });

    //Listar usuarios
    var table = null;
    var id_user = null;
    $(document).ready(function () {
        table = $('#users-table').DataTable({
            searching: false,
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{route('users.table')}}',
                statusCode: {
                    200: function (data) {
                        console.log(data);
                        $('.delete').on('click', function (event) {
                            var $element = $(this);
                            Swal.fire({
                                title: "¡Atención!",
                                text: "Una vez elimine no se podrán recuperar.",
                                icon: "warning",
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: "Sí, deseo eliminarlo",
                                cancelButtonText: "Volver",
                            }).then((willDelete) => {
                                if (willDelete.value) {
                                    $.ajax({
                                        headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                                        type: 'delete',
                                        url: '{{ route('users.destroy') }}/' + $element.data('id'),
                                        success: function (data) {
                                            Swal.fire(
                                                'Excelente!',
                                                'Se eliminó con éxito!',
                                                'success'
                                            );
                                            table.ajax.reload();
                                        },
                                        error: function () {
                                            Swal.fire({
                                                icon: 'error',
                                                title: 'Oops...',
                                                text: 'Algo salío mal',
                                            });
                                        }
                                    });
                                }
                            });
                        });
                        $('.edit').click(function () {
                            id_user = $(this).data('id');
                            $.ajax({
                                type: 'get',
                                url: '{{ route('users.get') }}/' + id_user,
                                success: function (user) {
                                    $('.edit-users #name').val(user.name);
                                    $('.edit-users #email').val(user.email);
                                    $('.edit-users #document').val(user.document);
                                    $('.edit-users #mobile').val(user.mobile);
                                    $('.edit-users #address').val(user.address);
                                    $('.edit-users #roles').val([]).change();
                                    $.each(user.roles, function (index, role) {
                                        $('.edit-users #roles').append(`<option value="${role.id}" selected>${role.display_name}</option>`);
                                    });
                                }
                            });
                        });
                    },
                },
            },
            columns: [
                {data: 'document', name: 'document'},
                {data: 'name', name: 'users.name'},
                {data: 'email', name: 'email'},
                {data: 'mobile', name: 'mobile'},
                {data: 'address', name: 'address'},
                {data: 'roles', name: 'roles'},
                {data: 'action', orderable: false}

            ],
            language: {
                "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
            }

        });

    });

    // Agregar usuarios
    $('.add-users').submit(function (e) {
        e.preventDefault();
        $.ajax({
            url: '{{route('users.store')}}',
            headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
            type: 'POST',
            dataType: "json",
            data: {
                document: $('.from-users-create #document').val(),
                name: $('.from-users-create #name').val(),
                email: $('.from-users-create #email').val(),
                password: $('.from-users-create #password').val(),
                password_confirmation: $('.from-users-create #password_confirmation').val(),
                mobile: $('.from-users-create #mobile').val(),
                address: $('.from-users-create #address').val(),
                roles: $('.from-users-create #roles_create').val(),
            },
            success: function (data) {
                if (data) {
                    //Limpiar el formulario
                    $('.add-users')[0].reset();
                    //Limpia el select 2
                    $('.from-users-create .roles').val(null).change();
                    //Elimina los span si hubo errores previos
                    if ($(".span-error")) {
                        $(".span-error").remove();
                    }
                    //Alerta de éxito en el registro
                    Swal.fire(
                        'Excelente!',
                        'Se guardó con éxito!',
                        'success'
                    );
                    //Cierra el modal
                    $('.from-users-create').modal('hide');
                    //Recarga la tabla
                    table.ajax.reload();
                }
            },
            error: function (err) {
                if (err.status == 422) { // Valida el estado de respuesta según la validación
                    $('#success_message').fadeIn().html(err.responseJSON.message); //Envia el mesaje de error

                    // Función para mostrar debajo del campo el mensaje de error personalizado
                    $.each(err.responseJSON.errors, function (i, error) {
                        var el = $(document).find('[name="' + i + '"]');
                        el.after($('<span style="color: red;" class="span-error">' + error[0] + '</span>'));
                    });
                }
                // Alerta de error
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Algo salío mal',
                });
            }
        });
    });

    // Actualizar usuarios
    $('.edit-users').submit(function (e) {
        e.preventDefault();
        $.ajax({
            url: '{{route('users.update')}}' + "/" + id_user,
            headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
            type: 'put',
            dataType: "json",
            data: {
                document: $('.from-users-edit #document').val(),
                name: $('.from-users-edit #name').val(),
                email: $('.from-users-edit #email').val(),
                password: $('.from-users-edit #password').val(),
                password_confirmation: $('.from-users-edit #password_confirmation').val(),
                mobile: $('.from-users-edit #mobile').val(),
                address: $('.from-users-edit #address').val(),
                roles: $('.from-users-edit #roles_edit').val(),
            },
            success: function (data) {
                if (data) {
                    //Limpiar el formulario
                    $('.edit-users')[0].reset();
                    //Limpia el select 2
                    $('.from-users-edit .roles').val(null).change();
                    //Elimina los span si hubo errores previos
                    if ($(".span-error")) {
                        $(".span-error").remove();
                    }
                    //Alerta de éxito en el registro
                    Swal.fire(
                        'Excelente!',
                        'Se guardó con éxito!',
                        'success'
                    );
                    //Cierra el modal
                    $('.from-users-edit').modal('hide');
                    //Recarga la tabla
                    table.ajax.reload();
                }
            },
            error: function (err) {
                if (err.status == 422) { // Valida el estado de respuesta según la validación
                    $('#success_message').fadeIn().html(err.responseJSON.message); //Envia el mesaje de error

                    // Función para mostrar debajo del campo el mensaje de error personalizado
                    $.each(err.responseJSON.errors, function (i, error) {
                        var el = $(document).find('[name="' + i + '"]');
                        el.after($('<span style="color: red;" class="span-error">' + error[0] + '</span>'));
                    });
                }
                // Alerta de error
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Algo salío mal',
                });
            }
        });
    });

    //Personalización de botón
    $('.btn-delete').on('click', function (event) {
        event.preventDefault();
        var form = ($(this).parent());
        Swal.fire({
            title: "¡Atención!",
            text: "¡Una vez borrado el registro no se podrá recuperar!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: "Sí, deseo borrarlo",
            cancelButtonText: "Volver",
        }).then(function (borrar) {
            if (borrar.value) {
                form.submit();
            } else {
                return false;
            }
        });
    });

</script>

</body>
</html>
