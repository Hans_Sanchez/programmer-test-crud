<div class="modal fade" id="CreateRole" tabindex="-1" role="dialog" aria-labelledby="CreateRoleLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="CreateRoleLabel">Nuevo rol</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('roles.store') }}" method="post" autocomplete="off">
                @csrf
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name"><strong>Nombre <span class="text-danger">*</span></strong></label>
                                    <input id="name" class="form-control" type="text" value="{{old('name')}}" name="name" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="display_name"><strong>Nombre en pantalla <span class="text-danger">*</span></strong></label>
                                    <input id="display_name" class="form-control" type="text" value="{{old('display_name')}}" name="display_name" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="description"><strong>Descripción</strong></label>
                                    <input id="description" class="form-control" type="text" value="{{old('description')}}" name="description" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">
                        Cancelar
                    </button>
                    <button type="submit" class="btn btn-success">
                        Guardar
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
