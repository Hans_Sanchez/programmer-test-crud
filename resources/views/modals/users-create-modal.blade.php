<div class="modal fade from-users-create" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1"
     aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">
                    <b>Nuevo Usuario</b>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">x</button>
            </div>
            <form class="add-users" method="post">
                @csrf
                <div class="modal-body">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name"><strong>Nombre <span class="text-danger">*</span>
                                        </strong></label>
                                    <input id="name" class="form-control" type="text" name="name"
                                           placeholder="Escribe tú nombre aquí" value="{{old('name')}}" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="email"><strong>Correo electrónico <span
                                                class="text-danger">*</span></strong></label>
                                    <input type="text" class="form-control" id="email"
                                           data-inputmask="'alias': 'email'" name="email"
                                           placeholder="Escribe tú correo electrónico aquí"
                                           value="{{old('email')}}" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="document "><strong>Número de documento <span
                                                class="text-danger">*</span></strong></label>
                                    <input type="text" class="form-control" name="document" id="document"
                                           data-inputmask-alias="9999999999"
                                           placeholder="Escribe tú número de documento aquí"
                                           value="{{old('document')}}" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="mobile"><strong>Número de teléfono</strong></label>
                                    <input id="mobile" class="form-control" type="text" name="mobile"
                                           placeholder="Escribe tú número de teléfono aquí"
                                           data-inputmask-alias="999-999-9999" value="{{old('mobile')}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="mobile"><strong>Dirección</strong></label>
                                    <input type="text" class="form-control" name="address" id="address"
                                           placeholder="Ejm. Calle 12 # 135 - 00" value="{{old('address')}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="roles"><strong>Rol o Cargo a ejercer
                                            <span class="text-danger">*</span></strong></label>
                                    <select name="roles"
                                            class="form-control roles" id="roles_create" required>
                                        <option value=""></option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="password"><strong>Contraseña <span class="text-danger">*</span></strong></label>
                                    <input id="password" class="form-control" type="password" name="password"
                                           placeholder="********" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="password_confirmation"><strong>Confirmar contraseña <span
                                                class="text-danger">*</span></strong></label>
                                    <input id="password_confirmation" class="form-control" type="password"
                                           name="password_confirmation"
                                           placeholder="********" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">
                        Cerrar
                    </button>
                    <button type="submit" class="btn btn-success">
                        Guardar
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
