<div class="modal fade" id="EditRole-{{ $role->id }}" tabindex="-1" role="dialog"
     aria-labelledby="EditRoleLabel-{{ $role->id }}" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="EditRoleLabel-{{ $role->id }}">Editar rol ({{ $role->display_name }})</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('roles.update', $role)}}" method="POST" autocomplete="off">
                @csrf
                @method('put')
                <div class="modal-body">
                    <div class="container">
                        <div class="row" style="text-align: left !important;">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name"><strong>Nombre <span class="text-danger">*</span></strong></label>
                                    <input type="hidden" name="name" value="{{$role->name}}">
                                    <input id="name" class="form-control" type="text"
                                           value="{{old('name', $role->name)}}" readonly disabled required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="display_name"><strong>Nombre en pantalla <span
                                                class="text-danger">*</span></strong></label>
                                    <input id="display_name" class="form-control" type="text"
                                           value="{{old('display_name', $role->display_name)}}"
                                           name="display_name"
                                           required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="description"><strong>Descripción</strong></label>
                                    <input id="description" class="form-control" type="text"
                                           value="{{old('description', $role->description ?: 'No se registró una descripción para este rol')}}"
                                           name="description"
                                           required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">
                        Cancelar
                    </button>
                    <button type="submit" class="btn btn-success">
                        Actualizar
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
