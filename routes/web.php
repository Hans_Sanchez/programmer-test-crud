<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Ruta principal - Welcome
Route::get('/', [\App\Http\Controllers\HomeController::class, 'welcome'])->name('welcome');

//Rutas para usuarios
Route::group(['prefix' => 'users'], function () {
    Route::get('/table', [\App\Http\Controllers\UserController::class, 'table'])->name('users.table');
    Route::post('/store', [\App\Http\Controllers\UserController::class, 'store'])->name('users.store');
    Route::get('/get/{user?}', [\App\Http\Controllers\UserController::class, 'get'])->name('users.get');
    Route::put('/update/{user?}', [\App\Http\Controllers\UserController::class, 'update'])->name('users.update');
    Route::delete('/destroy/{user?}', [\App\Http\Controllers\UserController::class, 'destroy'])->name('users.destroy');
});

//Rutas para roles o cargos
Route::group(['prefix' => 'roles'], function () {
    Route::get('/index', [\App\Http\Controllers\RoleController::class, 'index'])->name('roles.index');
    Route::post('/store', [\App\Http\Controllers\RoleController::class, 'store'])->name('roles.store');
    Route::put('/update/{role}', [\App\Http\Controllers\RoleController::class, 'update'])->name('roles.update');
    Route::delete('/destroy/{role}', [\App\Http\Controllers\RoleController::class, 'destroy'])->name('roles.destroy');
    Route::get('/searchRoles', [\App\Http\Controllers\RoleController::class, 'searchRole'])->name('roles.searchRoles');
});
