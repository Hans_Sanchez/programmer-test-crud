<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Creación de usuario por defecto
        DB::table('users')->delete();
        DB::table('users')->insert(array(
            0 =>
                array(
                    'document' => '1069766258',
                    'name' => 'Hans Yadiel Sanchez Mora',
                    'email' => 'hanssanhez427@gmail.com',
                    'email_verified_at' => NULL,
                    'password' => bcrypt('Hans123++'),
                    'mobile' => '3126248950',
                    'address' => 'Fusagasugá - Colombia',
                    'remember_token' => NULL,
                    'created_at' => now(),
                    'updated_at' => now(),
                ),
        ));

        //Creación de rol por defecto
        DB::table('roles')->delete();
        DB::table('roles')->insert(array(
            0 =>
                array(
                    'name' => 'admin',
                    'display_name' => 'Administrador',
                    'description' => 'Control total de la aplicación',
                    'created_at' => now(),
                    'updated_at' => now(),
                ),
        ));

        //Asiganación de rol a usuario por defecto
        DB::table('role_user')->delete();
        DB::table('role_user')->insert(array(
            0 =>
                array(
                    'user_id' => 1,
                    'role_id' => 1,
                ),
        ));

    }
}
